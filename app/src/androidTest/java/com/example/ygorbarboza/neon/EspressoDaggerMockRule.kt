package com.example.ygorbarboza.neon

import android.support.test.InstrumentationRegistry
import com.example.ygorbarboza.neon.di.AppComponentForTest
import com.example.ygorbarboza.neon.di.modules.AppModule
import com.example.ygorbarboza.neon.di.modules.NetworkModule
import com.example.ygorbarboza.neon.di.modules.RxJavaModule
import it.cosenonjaviste.daggermock.DaggerMock

fun espressoDaggerMockRule() = DaggerMock.rule<AppComponentForTest>(NetworkModule(), RxJavaModule()) {
    set { component -> component.inject(app) }
    customizeBuilder<AppComponentForTest.Builder> { it.application(app) }
}

val app: NeonApplication
    get() = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as NeonApplication