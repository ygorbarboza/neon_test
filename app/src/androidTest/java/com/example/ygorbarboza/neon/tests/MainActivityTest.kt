package com.example.ygorbarboza.neon.tests

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers.anyIntent
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.filters.SmallTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.example.ygorbarboza.neon.R
import com.example.ygorbarboza.neon.RxSchedulers
import com.example.ygorbarboza.neon.espressoDaggerMockRule
import com.example.ygorbarboza.neon.helper.assertVisible
import com.example.ygorbarboza.neon.helper.clickView
import com.example.ygorbarboza.neon.helper.intendingNoResponse
import com.example.ygorbarboza.neon.model.rest.RequestApiService
import com.example.ygorbarboza.neon.screen.main.MainActivity
import com.example.ygorbarboza.neon.screen.send_money_list.SendMoneyListActivity
import com.example.ygorbarboza.neon.screen.transfer_history.TransferHistoryActivity
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@SmallTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    var rule = espressoDaggerMockRule()

    @get:Rule
    val mainActivityRule = IntentsTestRule(MainActivity::class.java, false, false)

    val testScheduler = TestScheduler()

    val requestApiService: RequestApiService = mock()

    val rxSchedulers: RxSchedulers = spy(RxSchedulers())

    @Before
    fun setUp() {
        whenever(rxSchedulers.ioScheduler()).thenReturn(testScheduler)
    }


    @Test
    fun givenLoadedCorrectThenShowContent() {
        givenRequestSuccess()

        launchActivity()

        thenLoadingVisible()

        testScheduler.triggerActions()

        thenContentVisible()
    }

    @Test
    fun givenErrorThenShowError() {
        givenRequestError()

        launchActivity()

        thenLoadingVisible()

        testScheduler.triggerActions()

        thenErrorVisible()
    }

    @Test
    fun givenErrorAndClickRetryThenShowContent() {
        givenRequestError()

        launchActivity()

        testScheduler.triggerActions()

        givenRequestSuccess()

        clickView(R.id.retryBtn)

        thenLoadingVisible()

        testScheduler.triggerActions()

        thenContentVisible()
    }

    @Test
    fun givenLoadSuccessWhenClickTransferHistoryThenOpenTransferHistory() {
        givenRequestSuccess()

        launchActivity()

        testScheduler.triggerActions()

        intendingNoResponse(hasComponent(TransferHistoryActivity::class.java.name))

        clickView(R.id.transferHistoryBtn)

        Intents.intended(hasComponent(TransferHistoryActivity::class.java.name))
    }

    @Test
    fun givenLoadSuccessWhenClickSendMoneyThenOpenSentMoneyList() {
        givenRequestSuccess()

        launchActivity()

        testScheduler.triggerActions()

        intendingNoResponse(hasComponent(SendMoneyListActivity::class.java.name))

        clickView(R.id.sendMoneyBtn)

        Intents.intended(hasComponent(SendMoneyListActivity::class.java.name))
    }

    private fun launchActivity() {
        mainActivityRule.launchActivity(null)
    }

    private fun givenRequestSuccess() {
        whenever(requestApiService.generateToken(any(), any())).thenReturn(Single.just("Bla"))
    }

    private fun givenRequestError() {
        whenever(requestApiService.generateToken(any(), any())).thenReturn(Single.error(Exception()))
    }

    private fun thenContentVisible() {
        assertVisible(R.id.contentView)
        assertVisible(R.id.loadingView, false)
        assertVisible(R.id.errorView, false)
    }

    private fun thenLoadingVisible() {
        assertVisible(R.id.loadingView)
        assertVisible(R.id.contentView, false)
        assertVisible(R.id.errorView, false)
    }

    private fun thenErrorVisible() {
        assertVisible(R.id.errorView)
        assertVisible(R.id.contentView, false)
        assertVisible(R.id.loadingView, false)
    }

}