package com.example.ygorbarboza.neon.helper;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Checkable;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class Matchers {
  public static Matcher<View> withSize (final int size) {
    return new TypeSafeMatcher<View>() {
      @Override public boolean matchesSafely (final View view) {
        return ((RecyclerView) view).getAdapter().getItemCount() == size;
      }

      @Override public void describeTo (final Description description) {
        description.appendText ("RecyclerView should have " + size + " items");
      }
    };
  }
}
