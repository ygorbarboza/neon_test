package com.example.ygorbarboza.neon.helper

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.support.annotation.IdRes
import android.support.annotation.StringRes
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.*
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.v7.widget.RecyclerView
import com.example.ygorbarboza.neon.helper.Matchers.withSize
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not

fun clickView(@IdRes idRes: Int, @IdRes parentIdRes: Int) = onView(allOf(withParent(withId(parentIdRes)), withId(idRes), isDisplayed())).perform(click())

fun clickView(@IdRes idRes: Int) = onView(allOf(withId(idRes), isDisplayed())).perform(click())

fun clickViewWithText(text: String) = onView(allOf(withText(text), isDisplayed())).perform(click())

fun clickViewWithText(@StringRes stringRes: Int) = onView(allOf(withText(stringRes), isDisplayed())).perform(click())

fun clickOnItem(@IdRes recyclerViewId: Int, position: Int) = onView(allOf(withId(recyclerViewId), isDisplayed())).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(position, click()))

fun writeText(@IdRes editTextId: Int, text: String) = onView(allOf(withId(editTextId), isDisplayed())).perform(ViewActions.replaceText(text), ViewActions.closeSoftKeyboard())

fun checkForText(@IdRes textViewId: Int, text: String) = onView(allOf(withId(textViewId), isDisplayed())).check(ViewAssertions.matches(withText(text)))

fun checkForText(@IdRes parentId: Int, @IdRes textViewId: Int, text: String) {
    val parentMatcher = allOf(withId(parentId), isDisplayed())
    onView(allOf(isDescendantOfA(parentMatcher), withId(textViewId), isDisplayed())).check(ViewAssertions.matches(withText(text)))
}

fun checkForText(@IdRes textViewId: Int, @StringRes stringRes: Int) = onView(allOf(withId(textViewId), isDisplayed())).check(ViewAssertions.matches(withText(stringRes)))

fun checkForText(@StringRes stringRes: Int) = onView(allOf(withText(stringRes), isDisplayed())).check(ViewAssertions.matches(isDisplayed()))

fun swipe(direction: GeneralLocation, @IdRes id: Int) {
    val swipe = GeneralSwipeAction(Swipe.SLOW, GeneralLocation.CENTER,
            direction, Press.FINGER)

    onView(allOf(ViewMatchers.withId(id), isDisplayed())).perform(swipe, click())
}

fun assertEnable(@IdRes id: Int, isEnable: Boolean = true) {
    if (isEnable) {
        onView(allOf(withId(id), isDisplayed())).check(ViewAssertions.matches(isEnabled()))
    } else {
        onView(allOf(withId(id), isDisplayed())).check(ViewAssertions.matches(not(isEnabled())))
    }
}

fun assertVisible(@IdRes id: Int, isVisible: Boolean = true) {
    if (isVisible) {
        onView(allOf(withId(id))).check(ViewAssertions.matches(isDisplayed()))
    } else {
        onView(allOf(withId(id))).check(ViewAssertions.matches(not(isDisplayed())))
    }
}

fun assertListHasNumItems(@IdRes recyclerViewId: Int, count: Int) = onView(withId(recyclerViewId))
        .check(matches(withSize(count)))

fun intendingNoResponse(matcher: Matcher<Intent>) = Intents
        .intending(matcher)
        .respondWith(Instrumentation.ActivityResult(Activity.RESULT_OK, Intent()))
