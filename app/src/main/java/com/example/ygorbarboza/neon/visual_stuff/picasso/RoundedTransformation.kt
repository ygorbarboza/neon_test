package com.example.ygorbarboza.neon.visual_stuff.picasso

import android.graphics.*
import com.squareup.picasso.Transformation

class RoundedTransformation : Transformation {
    override fun transform(source: Bitmap): Bitmap {
        val size = Math.min(source.width, source.height)

        val centerX = (source.width - size) / 2
        val centerY = (source.height - size) / 2

        val squaredBitmap = Bitmap.createBitmap(source, centerX, centerY, size, size)
        if (squaredBitmap != source) {
            source.recycle()
        }

        val bitmap = Bitmap.createBitmap(size, size, source.config)
        val canvas = Canvas(bitmap)
        val paint = Paint().apply {
            shader = BitmapShader(squaredBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
            isAntiAlias = true
        }

        val r = size / 2f

        canvas.drawCircle(r, r, r, paint)
        squaredBitmap.recycle()

        return bitmap
    }

    override fun key(): String {
        return "circle"
    }

}