package com.example.ygorbarboza.neon.screen.send_money_detail

import android.app.Activity
import android.view.ViewGroup
import com.example.ygorbarboza.neon.model.entities.Person
import com.example.ygorbarboza.neon.screen.KEY_PERSON
import dagger.Binds
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_send_money_detail.*

@Module
abstract class SendMoneyDetailActivityModule {

    @Binds
    internal abstract fun provideActivity(activity: SendMoneyDetailActivity): Activity

    @Module
    companion object {
        @Provides
        @JvmStatic
        internal fun provideRootViewGroup(activity: SendMoneyDetailActivity): ViewGroup = activity.rootLayout

        @Provides
        @JvmStatic
        internal fun providePerson(activity: SendMoneyDetailActivity): Person = activity.intent.extras.getParcelable(KEY_PERSON)
    }

}