package com.example.ygorbarboza.neon.screen.transfer_history

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import com.example.ygorbarboza.neon.RxSchedulers
import com.example.ygorbarboza.neon.applySchedulerIO
import com.example.ygorbarboza.neon.ext.addToComposite
import com.example.ygorbarboza.neon.model.entities.Person
import com.example.ygorbarboza.neon.screen.pattern.presenter.Presenter
import com.example.ygorbarboza.neon.usecase.MoneyUseCase
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class TransferHistoryPresenter @Inject
constructor(viewBinder: TransferHistoryViewBinder,
            private val moneyUseCase: MoneyUseCase,
            private val rxSchedulers: RxSchedulers)
    : Presenter<TransferHistoryViewBinder>(viewBinder) {

    private val compositeDisposable = CompositeDisposable()

    init {
        requestPersonList()
    }

    private fun requestPersonList() {
        //Server seems to be down

        moneyUseCase
                .getTransfers()
                .applySchedulerIO(rxSchedulers)
                .doOnSubscribe { viewBinder.showLoading() }
                .subscribe({ showContent(it) }, { viewBinder.showError() })
                .addToComposite(compositeDisposable)
    }

    private fun showContent(persons: List<Person>) {
        val userOrderByAmount = persons.sortedByDescending { it.tansferAmount }
        val maxAmount = persons.maxBy { it.tansferAmount }!!.tansferAmount

        viewBinder.showContent(persons, userOrderByAmount, maxAmount)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        compositeDisposable.clear()
    }

}
