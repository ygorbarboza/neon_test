package com.example.ygorbarboza.neon.screen.main

import android.view.View
import android.view.ViewGroup
import com.example.ygorbarboza.neon.model.entities.Person
import com.example.ygorbarboza.neon.visual_stuff.picasso.NeonPicasso
import com.example.ygorbarboza.neon.screen.pattern.ViewBinder
import com.jakewharton.rxbinding2.view.clicks
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.layout_main_content.view.*
import kotlinx.android.synthetic.main.layout_main_error.view.*
import javax.inject.Inject

class MainViewBinder @Inject constructor(root: ViewGroup, private val neonPicasso: NeonPicasso) : ViewBinder(root) {

    fun showCurrentUser(user: Person) {
        root.run {
            loadingView.visibility = View.GONE
            errorView.visibility = View.GONE
            contentView.visibility = View.VISIBLE

            nameTextView.text = user.name
            emailTextView.text = user.email

            neonPicasso
                    .loadPicture(user.photoUrl)
                    .into(imageView)
        }
    }

    fun showError() {
        root.run {
            loadingView.visibility = View.GONE
            errorView.visibility = View.VISIBLE
            contentView.visibility = View.GONE
        }
    }

    fun showLoading() {
        root.run {
            loadingView.visibility = View.VISIBLE
            errorView.visibility = View.GONE
            contentView.visibility = View.GONE
        }
    }

    val onSendMoneyButtonClick = root.sendMoneyBtn.clicks().share()
    val onTransferHistoryButtonClick = root.transferHistoryBtn.clicks().share()

    val onRetryButtonClick = root.retryBtn.clicks().share()

}
