package com.example.ygorbarboza.neon.screen.send_money_list

import android.app.Activity
import android.view.ViewGroup
import dagger.Binds
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_send_money_list.*

@Module
abstract class SendMoneyListActivityModule {

    @Binds
    internal abstract fun provideActivity(activity: SendMoneyListActivity): Activity

    @Module
    companion object {

        @Provides
        @JvmStatic
        internal fun provideRootViewGroup(activity: SendMoneyListActivity): ViewGroup = activity.rootLayout

        @Provides
        @JvmStatic
        internal fun provideRecyclerView(activity: SendMoneyListActivity) = activity.recycler_view
    }

}