package com.example.ygorbarboza.neon.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.LinearGradient
import android.graphics.Paint
import android.graphics.Paint.Style
import android.graphics.Shader
import android.support.annotation.FloatRange
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import com.example.ygorbarboza.neon.R

class GraphView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {


    private val startColor = ContextCompat.getColor(context, R.color.graphItemColorStart)
    private val endColor = ContextCompat.getColor(context, R.color.graphItemColorEnd)
    private val graphWidth = resources.getDimensionPixelSize(R.dimen.graph_line_width)
    private val circleRadius = resources.getDimensionPixelSize(R.dimen.graph_circle_radius)

    private val graphPaint = Paint().apply {
        style = Style.STROKE
        strokeWidth = graphWidth.toFloat()
        isAntiAlias = true
    }

    private val circlePaint = Paint().apply {
        style = Style.FILL
        isAntiAlias = true
        color = endColor
    }

    private var graphHeightPercentage = 1.0f

    override fun onDraw(canvas: Canvas) {
        updateShader()

        canvas.drawGraphLine()

        canvas.drawTopGraph()
    }

    private fun Canvas.drawGraphLine() {
        val centerX = width / 2f
        val startY = height.toFloat() * (1 - graphHeightPercentage)
        val endY = height.toFloat()

        drawLine(centerX, startY, centerX, endY, graphPaint)
    }

    private fun updateShader() {
        graphPaint.shader =
                LinearGradient(0f,
                        height.toFloat(),
                        0f,
                        height.toFloat() * (1 - graphHeightPercentage),
                        startColor,
                        endColor,
                        Shader.TileMode.MIRROR)
    }

    private fun Canvas.drawTopGraph() {
        val centerX = width / 2f
        // Calculate possible area without radius to not go over screen,
        // get the height with the percentage, and add the a radius back to be on the center
        val centerY =
                (height.toFloat() - circleRadius * 2) * (1 - graphHeightPercentage) + circleRadius

        drawCircle(
                centerX,
                centerY,
                circleRadius.toFloat(),
                circlePaint)
    }

    fun setGraphHeight(@FloatRange(from = 0.00, to = 1.00) newGraphPercentage: Float) {
        graphHeightPercentage = newGraphPercentage
        invalidate()
    }

}