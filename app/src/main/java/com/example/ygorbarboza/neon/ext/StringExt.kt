package com.example.ygorbarboza.neon.ext

fun String.removeNonNumbers() = Regex("[^0-9]").replace(this, "")