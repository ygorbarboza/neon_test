package com.example.ygorbarboza.neon.screen.pattern.presenter

import android.arch.lifecycle.Lifecycle

abstract class PresenterHolder(lifecycle: Lifecycle) : LifecyclePresenter() {

    abstract val presenters: Array<LifecyclePresenter>

    init {
        presenters.forEach { lifecycle.addObserver(it) }
    }

}
