package com.example.ygorbarboza.neon.di.modules

import com.example.ygorbarboza.neon.screen.main.MainActivity
import com.example.ygorbarboza.neon.screen.main.MainActivityModule
import com.example.ygorbarboza.neon.screen.send_money_detail.SendMoneyDetailActivity
import com.example.ygorbarboza.neon.screen.send_money_detail.SendMoneyDetailActivityModule
import com.example.ygorbarboza.neon.screen.send_money_list.SendMoneyListActivity
import com.example.ygorbarboza.neon.screen.send_money_list.SendMoneyListActivityModule
import com.example.ygorbarboza.neon.screen.transfer_history.TransferHistoryActivity
import com.example.ygorbarboza.neon.screen.transfer_history.TransferHistoryActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(MainActivityModule::class), ActivityModule::class])
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [(SendMoneyListActivityModule::class), ActivityModule::class])
    internal abstract fun bindSendMoneyListActivity(): SendMoneyListActivity

    @ContributesAndroidInjector(modules = [(TransferHistoryActivityModule::class), ActivityModule::class])
    internal abstract fun bindTransferHistoryActivity(): TransferHistoryActivity

    @ContributesAndroidInjector(modules = [(SendMoneyDetailActivityModule::class), ActivityModule::class])
    internal abstract fun bindSendMoneyDetailActivity(): SendMoneyDetailActivity

}