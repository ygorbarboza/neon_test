package com.example.ygorbarboza.neon.screen

import android.app.Activity
import android.content.Intent
import com.example.ygorbarboza.neon.model.entities.Person
import com.example.ygorbarboza.neon.screen.send_money_detail.SendMoneyDetailActivity
import com.example.ygorbarboza.neon.screen.send_money_list.SendMoneyListActivity
import com.example.ygorbarboza.neon.screen.transfer_history.TransferHistoryActivity
import javax.inject.Inject

const val REQUEST_CODE_SEND_MONEY = 13253
const val KEY_PERSON = "person"

class Navigation @Inject constructor(private val activity: Activity) {

    fun goToSendMoneyList(){
        val intent = Intent(activity, SendMoneyListActivity::class.java)

        activity.startActivity(intent)
    }

    fun goToSendMoneyDetail(person: Person){
        val intent = Intent(activity, SendMoneyDetailActivity::class.java).apply {
            putExtra(KEY_PERSON, person)
        }

        activity.startActivityForResult(intent, REQUEST_CODE_SEND_MONEY)
    }

    fun goToTransferHistory(){
        val intent = Intent(activity, TransferHistoryActivity::class.java)

        activity.startActivity(intent)
    }

    fun close(){
        activity.finish()
    }

    fun closeWithResponse(intentCode: (Intent.() -> Unit) = {  }){
        val intent = Intent().also(intentCode)
        activity.setResult(Activity.RESULT_OK, intent)
        activity.finish()
    }

}