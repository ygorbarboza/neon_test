package com.example.ygorbarboza.neon

import com.example.ygorbarboza.neon.model.entities.Person

val MAIN_USER = Person(
        id = 0,
        name = "Ygor Barboza Muniz",
        email = "ygorbarboza@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://media.licdn.com/dms/image/C4D03AQExyA6gtZaZGw/profile-displayphoto-shrink_100_100/0?e=1531958400&v=beta&t=XbZrg2Vb2W2e1RfIXYAB5xeS-zEslRPezCgAePLE92Q"
)


val SEND_MONEY_USER = arrayOf(Person(
        id = 0,
        name = "Cara 1",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://media.licdn.com/dms/image/C4D03AQExyA6gtZaZGw/profile-displayphoto-shrink_100_100/0?e=1531958400&v=beta&t=XbZrg2Vb2W2e1RfIXYAB5xeS-zEslRPezCgAePLE92Q"
), Person(
        id = 1,
        name = "Cara 1",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSaep65FiYmOJYvIdy1795CV-4x5eMtu6nNawpka0wpzMG0r21DRg",
        tansferAmount = 2.0
), Person(
        id = 2,
        name = "Cara 2",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKbH960igYqhrejKuPxgCf07e8bHg5KgKo3Orp4ey1xZvFsxJD",
        tansferAmount = 3.0
), Person(
        id = 3,
        name = "Cara 3",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRbzZsyaXcwLb8lqOHwICmv5SNmJ1mqv-BWj6nwXkxC-_hVTns0vg",
        tansferAmount = 4.0
), Person(
        id = 4,
        name = "Cara 4",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT_he3il8gADsTFHIEnZgmYLHe9aeH_StgD_YApB6_560NlZAk8",
        tansferAmount = 5.0
), Person(
        id = 5,
        name = "Cara 5",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCvYXGexL_eVm7q_W64YpVa7gm7FxXbQiJ7qzMMb64Aem523MY",
        tansferAmount = 6.0
), Person(
        id = 6,
        name = "Cara 6",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiBrHXZ3IhtedwdB1dhtohQVz6s2v8htkwUttkg2uNpHRokS81lw",
        tansferAmount = 7.0
), Person(
        id = 7,
        name = "Cara 7",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjhs0jxseqa1cM9rVRAfJ6LT-MZEUkepWWDkDorXjKX1cHEaTi",
        tansferAmount = 8.0
), Person(
        id = 8,
        name = "Cara 8",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTA1h-DGh0dLhZgmIMe6j0N3ldYcUunaEqu-1-5BWbwcXfMjHrmkQ",
        tansferAmount = 9.0
), Person(
        id = 9,
        name = "Cara 9",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://media.licdn.com/dms/image/C4D03AQExyA6gtZaZGw/profile-displayphoto-shrink_100_100/0?e=1531958400&v=beta&t=XbZrg2Vb2W2e1RfIXYAB5xeS-zEslRPezCgAePLE92Q",
        tansferAmount = 10.0
), Person(
        id = 10,
        name = "Cara 10",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://media.licdn.com/dms/image/C4D03AQExyA6gtZaZGw/profile-displayphoto-shrink_100_100/0?e=1531958400&v=beta&t=XbZrg2Vb2W2e1RfIXYAB5xeS-zEslRPezCgAePLE92Q",
        tansferAmount = 11.0
), Person(
        id = 11,
        name = "Cara 11",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://media.licdn.com/dms/image/C4D03AQExyA6gtZaZGw/profile-displayphoto-shrink_100_100/0?e=1531958400&v=beta&t=XbZrg2Vb2W2e1RfIXYAB5xeS-zEslRPezCgAePLE92Q",
        tansferAmount = 12.0
), Person(
        id = 12,
        name = "Cara 12",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://media.licdn.com/dms/image/C4D03AQExyA6gtZaZGw/profile-displayphoto-shrink_100_100/0?e=1531958400&v=beta&t=XbZrg2Vb2W2e1RfIXYAB5xeS-zEslRPezCgAePLE92Q",
        tansferAmount = 13.0
), Person(
        id = 13,
        name = "Cara 13",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://media.licdn.com/dms/image/C4D03AQExyA6gtZaZGw/profile-displayphoto-shrink_100_100/0?e=1531958400&v=beta&t=XbZrg2Vb2W2e1RfIXYAB5xeS-zEslRPezCgAePLE92Q",
        tansferAmount = 14.0
), Person(
        id = 14,
        name = "Cara 14",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://media.licdn.com/dms/image/C4D03AQExyA6gtZaZGw/profile-displayphoto-shrink_100_100/0?e=1531958400&v=beta&t=XbZrg2Vb2W2e1RfIXYAB5xeS-zEslRPezCgAePLE92Q",
        tansferAmount = 15.0
), Person(
        id = 15,
        name = "Cara 15",
        email = "cara@gmail.com",
        phone = "(21)99498-7030",
        photoUrl = "https://media.licdn.com/dms/image/C4D03AQExyA6gtZaZGw/profile-displayphoto-shrink_100_100/0?e=1531958400&v=beta&t=XbZrg2Vb2W2e1RfIXYAB5xeS-zEslRPezCgAePLE92Q",
        tansferAmount = 16.0
))