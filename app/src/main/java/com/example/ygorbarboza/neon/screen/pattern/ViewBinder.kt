package com.example.ygorbarboza.neon.screen.pattern

import android.view.ViewGroup

abstract class ViewBinder(protected val root: ViewGroup)
