package com.example.ygorbarboza.neon.model.rest

import com.example.ygorbarboza.neon.model.entities.Person
import com.example.ygorbarboza.neon.model.entities.Transfer
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RequestApiService {

    @GET("GenerateToken")
    fun generateToken(@Query("nome") name: String,
                      @Query("email") email: String):
            Single<String>

    @POST("SendMoney")
    fun sendMoney(@Query("ClientId") clientId: Int,
                      @Query("token") token: String,
                      @Query("valor") valor: Double):
            Single<Boolean>

    @GET("GetTransfers")
    fun getTransfers(@Query("token") token: String):
            Single<List<Person>>

}