package com.example.ygorbarboza.neon.di.components

import com.example.ygorbarboza.neon.NeonApplication
import com.example.ygorbarboza.neon.di.modules.ActivityBuilder
import com.example.ygorbarboza.neon.di.modules.AppModule
import com.example.ygorbarboza.neon.di.modules.NetworkModule
import com.example.ygorbarboza.neon.di.modules.RxJavaModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules =
[(AppModule::class),
    (RxJavaModule::class),
    (AppModule::class),
    (AndroidSupportInjectionModule::class),
    (ActivityBuilder::class),
    (NetworkModule::class)
])
interface AppComponent : AndroidInjector<NeonApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: NeonApplication): Builder

        fun build(): AppComponent
    }

}
