package com.example.ygorbarboza.neon.screen.transfer_history

import android.app.Activity
import android.view.ViewGroup
import dagger.Binds
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_transfer_history.*

@Module
abstract class TransferHistoryActivityModule {

    @Binds
    internal abstract fun provideActivity(activity: TransferHistoryActivity): Activity

    @Module
    companion object {
        @Provides
        @JvmStatic
        internal fun provideRootViewGroup(activity: TransferHistoryActivity): ViewGroup = activity.rootLayout
    }

}