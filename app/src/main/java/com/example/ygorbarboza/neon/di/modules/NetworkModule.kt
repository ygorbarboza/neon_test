package com.example.ygorbarboza.neon.di.modules

import android.app.Application
import com.example.ygorbarboza.neon.model.rest.RequestApiService
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

private const val URL = "http://processoseletivoneon.neonhomol.com.br"

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun requestApiService() =
            Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(URL)
                    .build()
                    .create(RequestApiService::class.java)

    @Provides
    fun picasso(application: Application) = Picasso
            .Builder(application)
            .build()

}
