package com.example.ygorbarboza.neon.screen.main

import android.app.Activity
import android.view.ViewGroup
import dagger.Binds
import dagger.Module
import dagger.Provides
import kotlinx.android.synthetic.main.activity_main.*

@Module
abstract class MainActivityModule {

    @Binds
    internal abstract fun provideActivity(mainActivity: MainActivity): Activity

    @Module
    companion object {
        @Provides
        @JvmStatic
        internal fun provideRootViewGroup(activity: MainActivity): ViewGroup = activity.rootLayout
    }

}