package com.example.ygorbarboza.neon.usecase

import com.example.ygorbarboza.neon.model.rest.RequestApiService
import com.example.ygorbarboza.neon.model.storage.TokenStorage
import javax.inject.Inject

private const val DUMMY_NAME = "Teste da Silva"
private const val DUMMY_EMAIL = "vem@paraoneon.com.br"

class TokenUseCase @Inject constructor(private val requestApiService: RequestApiService,
                                       private val tokenStorage: TokenStorage) {

    fun requestNewToken() =
            requestApiService
                        .generateToken(DUMMY_NAME, DUMMY_EMAIL)
                        .doOnSuccess { tokenStorage.saveToken(it) }

}