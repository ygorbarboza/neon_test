package com.example.ygorbarboza.neon.screen.send_money_list

import android.app.Activity
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Intent
import com.example.ygorbarboza.neon.SEND_MONEY_USER
import com.example.ygorbarboza.neon.ext.addToComposite
import com.example.ygorbarboza.neon.screen.Navigation
import com.example.ygorbarboza.neon.screen.pattern.presenter.Presenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SendMoneyListPresenter @Inject constructor(
        viewBinder: SendMoneyListViewBinder,
        private val navigation: Navigation)
    : Presenter<SendMoneyListViewBinder>(viewBinder) {

    private val compositeDisposable = CompositeDisposable()

    init {
        viewBinder
                .onItemClick
                .subscribe { position ->
                    navigation.goToSendMoneyDetail(SEND_MONEY_USER[position])
                }
                .addToComposite(compositeDisposable)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        compositeDisposable.clear()
    }

    fun onResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) navigation.close()
    }

}
