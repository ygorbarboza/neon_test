package com.example.ygorbarboza.neon

import com.example.ygorbarboza.neon.ext.removeNonNumbers
import java.text.NumberFormat
import javax.inject.Inject

class MoneyParser @Inject constructor(private val numberFormat: NumberFormat) {

    fun transformIntoDouble(value: String) =
            numberFormat.parse(value).toDouble()

    fun format(value: Double) = numberFormat.format(value)

}