package com.example.ygorbarboza.neon.screen.send_money_detail

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import com.example.ygorbarboza.neon.MoneyParser
import com.example.ygorbarboza.neon.RxSchedulers
import com.example.ygorbarboza.neon.applySchedulerIO
import com.example.ygorbarboza.neon.ext.addToComposite
import com.example.ygorbarboza.neon.model.entities.Person
import com.example.ygorbarboza.neon.screen.Navigation
import com.example.ygorbarboza.neon.usecase.MoneyUseCase
import com.example.ygorbarboza.neon.screen.pattern.presenter.Presenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SendMoneyDetailPresenter @Inject constructor(
        viewBinder: SendMoneyDetailViewBinder,
        private val navigation: Navigation,
        private val person: Person,
        private val moneyUseCase: MoneyUseCase,
        private val rxSchedulers: RxSchedulers,
        private val moneyParser: MoneyParser) : Presenter<SendMoneyDetailViewBinder>(viewBinder) {

    private val compositeDisposable = CompositeDisposable()

    init {
        viewBinder.setCurrentUser(person)

        viewBinder
                .onSendMoneyButtonClick
                .subscribe { sendMoney() }
                .addToComposite(compositeDisposable)

        viewBinder
                .onCloseClick
                .subscribe { navigation.close() }
                .addToComposite(compositeDisposable)
    }

    private fun sendMoney() {
        val value = retrieveValue()

        if (value == 0.00) {
            viewBinder.showNoMoneyError()
            return
        }

        moneyUseCase
                .sendMoney(person, retrieveValue())
                .applySchedulerIO(rxSchedulers)
                .doOnSubscribe { viewBinder.showLoading() }
                .subscribe({ returnResponse() }, { viewBinder.showError() })
                .addToComposite(compositeDisposable)
    }

    private fun returnResponse() {
        navigation.closeWithResponse()
    }

    private fun retrieveValue() = moneyParser.transformIntoDouble(viewBinder.getValueText().toString())

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        compositeDisposable.clear()
    }

}
