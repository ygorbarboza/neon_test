package com.example.ygorbarboza.neon.screen.pattern.activity

import android.os.Bundle
import com.example.ygorbarboza.neon.screen.pattern.presenter.LifecyclePresenter
import dagger.android.AndroidInjection

abstract class LegoControllerActivity<T: LifecyclePresenter> : LegoActivity() {

    abstract var presenter: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        lifecycle.addObserver(presenter)
    }

}
