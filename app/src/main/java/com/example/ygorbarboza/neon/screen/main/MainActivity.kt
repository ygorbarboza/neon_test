package com.example.ygorbarboza.neon.screen.main

import com.example.ygorbarboza.neon.R
import com.example.ygorbarboza.neon.screen.pattern.activity.LegoControllerActivity
import javax.inject.Inject

class MainActivity : LegoControllerActivity<MainPresenter>() {

    override val layoutId = R.layout.activity_main

    @Inject override lateinit var presenter: MainPresenter

}