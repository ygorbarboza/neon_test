package com.example.ygorbarboza.neon.model.storage

import android.content.SharedPreferences
import javax.inject.Inject

private const val KEY_TOKEN = "token"
const val EMPTY_TOKEN = "token"

class TokenStorage @Inject constructor(
        private val sharedPreferences: SharedPreferences
) {

    fun saveToken(token: String) = sharedPreferences.edit().apply {
        putString(KEY_TOKEN, token)
        commit()
    }

    fun getToken(): String? {
        val token = sharedPreferences.getString(KEY_TOKEN, EMPTY_TOKEN)
        return if (token == EMPTY_TOKEN) null else token
    }

}