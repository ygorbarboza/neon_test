package com.example.ygorbarboza.neon.screen.pattern.activity

import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_send_money_list.*

abstract class LegoActivity : AppCompatActivity() {

    abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)

        if (toolbar != null) {
            setupNeonToolbar()
        }
    }

    protected open val hasUpNavigation = false

    private fun setupNeonToolbar() {
        toolbar.setOnBackListener(View.OnClickListener { navigateUp() })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId

        if (itemId == android.R.id.home) {
            if (navigateUp()) return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun navigateUp(): Boolean {
        try {
            NavUtils.navigateUpFromSameTask(this)
            return true
        } catch (e: Exception) {
            e.printStackTrace()
            onBackPressed()
        }
        return false
    }

}
