package com.example.ygorbarboza.neon.screen.send_money_detail

import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.ygorbarboza.neon.R
import com.example.ygorbarboza.neon.model.entities.Person
import com.example.ygorbarboza.neon.visual_stuff.picasso.NeonPicasso
import com.example.ygorbarboza.neon.screen.pattern.ViewBinder
import com.example.ygorbarboza.neon.visual_stuff.edittext.MoneyMaskWatcher
import com.jakewharton.rxbinding2.view.clicks
import kotlinx.android.synthetic.main.activity_send_money_detail.view.*
import javax.inject.Inject

class SendMoneyDetailViewBinder @Inject constructor(root: ViewGroup, private val neonPicasso: NeonPicasso, moneyMaskWatcher: MoneyMaskWatcher) : ViewBinder(root) {

    val onSendMoneyButtonClick = root.sendMoneyBtn.clicks().share()
    val onCloseClick = root.closeIV.clicks().share()

    init {
        root.valueET.apply {
            addTextChangedListener(moneyMaskWatcher)
            setText("0")
        }

    }

    fun setCurrentUser(person: Person) {
        root.run {
            nameTV.text = person.name
            emailTV.text = person.email

            neonPicasso
                    .loadPicture(person.photoUrl)
                    .into(imageView)
        }
    }

    fun getValueText() = root.valueET.text

    fun showLoading() {
        root.run {
            progressBar.visibility = View.VISIBLE
            sendMoneyBtn.visibility = View.GONE
            valueET.isEnabled = false
        }
    }

    fun showError() {
        root.run {
            progressBar.visibility = View.GONE
            sendMoneyBtn.visibility = View.VISIBLE
            valueET.isEnabled = true
        }

        showErrorMessage()
    }

    private fun showErrorMessage() = Toast.makeText(root.context, R.string.send_money_detail_activity_error_message, Toast.LENGTH_SHORT).show()

    fun showNoMoneyError() {
        Toast.makeText(root.context, R.string.send_money_detail_activity_no_money_error_message, Toast.LENGTH_SHORT).show()
    }

}
