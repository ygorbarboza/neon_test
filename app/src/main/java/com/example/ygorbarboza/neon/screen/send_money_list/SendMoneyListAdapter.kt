package com.example.ygorbarboza.neon.screen.send_money_list

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.ygorbarboza.neon.R
import com.example.ygorbarboza.neon.SEND_MONEY_USER
import com.example.ygorbarboza.neon.model.entities.Person
import com.example.ygorbarboza.neon.visual_stuff.picasso.NeonPicasso
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.detaches
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_person.view.*
import javax.inject.Inject

class SendMoneyListAdapter @Inject constructor(
        private val context: Context,
        private val neonPicasso: NeonPicasso,
        private val recyclerView: RecyclerView) :
        Adapter<SendMoneyListAdapter.PersonHolder>() {

    private val clickSubject = PublishSubject.create<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_person, parent, false)
        return PersonHolder(view)
    }

    override fun onBindViewHolder(holder: PersonHolder, position: Int) {
        holder.bindAlarm(SEND_MONEY_USER[position])
    }

    override fun getItemCount() = SEND_MONEY_USER.size

    val itemClickListener = clickSubject.filter { it >= 0 }

    inner class PersonHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView
                    .clicks()
                    .map { adapterPosition }
                    .takeUntil(recyclerView.detaches())
                    .subscribe(clickSubject)
        }

        fun bindAlarm(person: Person) {
            with(itemView) {
                nameTextView.text = person.name
                phoneTextView.text = person.phone
                neonPicasso.loadPicture(person.photoUrl).into(pictureIV)
            }
        }

    }

}
