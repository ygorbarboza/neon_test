package com.example.ygorbarboza.neon.screen.pattern.presenter

import com.example.ygorbarboza.neon.screen.pattern.ViewBinder


abstract class Presenter<out VB : ViewBinder>(val viewBinder: VB) : LifecyclePresenter()
