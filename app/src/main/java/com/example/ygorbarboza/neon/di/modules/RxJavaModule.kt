package com.example.ygorbarboza.neon.di.modules

import com.example.ygorbarboza.neon.RxSchedulers
import dagger.Module
import dagger.Provides

@Module
class RxJavaModule {

    @Provides
    fun requestRxSchedulers() = RxSchedulers()

}
