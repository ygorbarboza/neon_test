package com.example.ygorbarboza.neon.screen.send_money_list

import android.content.Intent
import com.example.ygorbarboza.neon.R
import com.example.ygorbarboza.neon.screen.pattern.activity.LegoControllerActivity
import javax.inject.Inject

class SendMoneyListActivity : LegoControllerActivity<SendMoneyListPresenter>() {

    override val layoutId = R.layout.activity_send_money_list

    @Inject
    override lateinit var presenter: SendMoneyListPresenter

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onResult(requestCode, resultCode, data)
    }

}