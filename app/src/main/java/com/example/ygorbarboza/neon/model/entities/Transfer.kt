package com.example.ygorbarboza.neon.model.entities

import java.util.*

data class Transfer(
        val id: Int,
        val clientId: Int,
        val value: Double,
        val token: String,
        val date: Date)