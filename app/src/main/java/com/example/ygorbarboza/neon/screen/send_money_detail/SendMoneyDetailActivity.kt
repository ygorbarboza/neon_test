package com.example.ygorbarboza.neon.screen.send_money_detail

import com.example.ygorbarboza.neon.R
import com.example.ygorbarboza.neon.screen.pattern.activity.LegoControllerActivity
import javax.inject.Inject

class SendMoneyDetailActivity : LegoControllerActivity<SendMoneyDetailPresenter>() {

    override val layoutId = R.layout.activity_send_money_detail

    @Inject override lateinit var presenter: SendMoneyDetailPresenter

}