package com.example.ygorbarboza.neon.screen.transfer_history

import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import com.example.ygorbarboza.neon.R
import com.example.ygorbarboza.neon.R.id.loadingView
import com.example.ygorbarboza.neon.model.entities.Person
import com.example.ygorbarboza.neon.screen.pattern.ViewBinder
import kotlinx.android.synthetic.main.activity_transfer_history.view.*
import kotlinx.android.synthetic.main.layout_transfer_history_content.view.*
import javax.inject.Inject

class TransferHistoryViewBinder @Inject constructor(
        root: ViewGroup,
        private val transferHistoryListAdapter: TransferHistoryListAdapter,
        private val transferHistoryGraphAdapter: TransferHistoryGraphAdapter)
    : ViewBinder(root) {

    init {
        configureListView()
        configureGraphView()
    }

    private fun configureListView() {
        val divider = DividerItemDecoration(root.context, LinearLayoutManager.VERTICAL).apply {
            setDrawable(ContextCompat.getDrawable(root.context, R.drawable.divider_list)!!)
        }

        with(root.listRV) {
            layoutManager = LinearLayoutManager(root.context, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(divider)
            adapter = transferHistoryListAdapter
        }
    }

    private fun configureGraphView() {
        with(root.graphRV) {
            layoutManager = LinearLayoutManager(root.context, LinearLayoutManager.HORIZONTAL, false)
            adapter = transferHistoryGraphAdapter
        }
    }

    fun showContent(person: List<Person>, userOrderByAmount: List<Person>, maxAmount: Double){
        root.run {
            loadingView.visibility = View.GONE
            errorView.visibility = View.GONE
            contentView.visibility = View.VISIBLE
        }

        transferHistoryListAdapter.updateContent(person)
        transferHistoryGraphAdapter.updateContent(userOrderByAmount, maxAmount)
    }

    fun showError() {
        root.run {
            loadingView.visibility = View.GONE
            errorView.visibility = View.VISIBLE
            contentView.visibility = View.GONE
        }
    }

    fun showLoading() {
        root.run {
            loadingView.visibility = View.VISIBLE
            errorView.visibility = View.GONE
            contentView.visibility = View.GONE
        }
    }

}
