package com.example.ygorbarboza.neon.usecase

import com.example.ygorbarboza.neon.model.entities.Person
import com.example.ygorbarboza.neon.model.rest.RequestApiService
import com.example.ygorbarboza.neon.model.storage.TokenStorage
import javax.inject.Inject

class MoneyUseCase @Inject constructor(private val requestApiService: RequestApiService,
                                       private val tokenStorage: TokenStorage) {

    fun sendMoney(person: Person, value: Double) =
            requestApiService
                    .sendMoney(person.id,
                            tokenStorage.getToken()!!,
                            value)

    fun getTransfers() =
            requestApiService
                    .getTransfers(
                            tokenStorage.getToken()!!)

}