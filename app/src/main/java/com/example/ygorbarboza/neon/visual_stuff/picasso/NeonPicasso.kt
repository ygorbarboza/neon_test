package com.example.ygorbarboza.neon.visual_stuff.picasso

import com.example.ygorbarboza.neon.R
import com.squareup.picasso.Picasso
import javax.inject.Inject

class NeonPicasso @Inject constructor(private val picasso: Picasso) {
    fun loadPicture(url: String) =
            picasso
                    .load(url)
                    .placeholder(R.drawable.img_placeholder)
                    .transform(RoundedTransformation())
}