package com.example.ygorbarboza.neon.screen.transfer_history

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.ygorbarboza.neon.MoneyParser
import com.example.ygorbarboza.neon.R
import com.example.ygorbarboza.neon.SEND_MONEY_USER
import com.example.ygorbarboza.neon.model.entities.Person
import com.example.ygorbarboza.neon.visual_stuff.picasso.NeonPicasso
import kotlinx.android.synthetic.main.item_transfer_graph.view.*
import javax.inject.Inject

class TransferHistoryGraphAdapter @Inject constructor(
        private val context: Context,
        private val neonPicasso: NeonPicasso,
        private val moneyParser: MoneyParser) :
        Adapter<TransferHistoryGraphAdapter.PersonHolder>() {

    private var currentPersons: List<Person>? = null
    private var currentMaxAmount: Double = 100.0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_transfer_graph, parent, false)
        return PersonHolder(view)
    }

    override fun onBindViewHolder(holder: PersonHolder, position: Int) {
        holder.bindAlarm(currentPersons!![position])
    }

    fun updateContent(newPersons: List<Person>, maxAmount: Double) {
        currentPersons = newPersons
        currentMaxAmount = maxAmount
        notifyDataSetChanged()
    }

    override fun getItemCount() = currentPersons?.size ?: 0

    inner class PersonHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindAlarm(person: Person) {
            with(itemView) {
                valueTV.text = moneyParser.format(person.tansferAmount)
                graphGF.setGraphHeight((person.tansferAmount / currentMaxAmount).toFloat())
//                phoneTextView.text = person.phone
                neonPicasso.loadPicture(person.photoUrl).into(pictureIV)
            }
        }

    }

}
