package com.example.ygorbarboza.neon.screen.send_money_list

import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.ViewGroup
import com.example.ygorbarboza.neon.R
import com.example.ygorbarboza.neon.screen.pattern.ViewBinder
import kotlinx.android.synthetic.main.activity_send_money_list.view.*
import javax.inject.Inject

class SendMoneyListViewBinder @Inject constructor(root: ViewGroup, private val currentAdapter: SendMoneyListAdapter) : ViewBinder(root) {

    val onItemClick = currentAdapter.itemClickListener

    init {
        val divider = DividerItemDecoration(root.context, LinearLayoutManager.VERTICAL).apply {
            setDrawable(ContextCompat.getDrawable(root.context, R.drawable.divider_list)!!)
        }

        with(root.recycler_view) {
            layoutManager = LinearLayoutManager(root.context, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(divider)
            adapter = currentAdapter
        }
    }

}
