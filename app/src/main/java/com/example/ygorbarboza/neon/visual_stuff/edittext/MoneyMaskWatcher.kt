package com.example.ygorbarboza.neon.visual_stuff.edittext

import android.text.Editable
import android.text.TextWatcher
import com.example.ygorbarboza.neon.MoneyParser
import com.example.ygorbarboza.neon.ext.removeNonNumbers
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

class MoneyMaskWatcher @Inject constructor(private val moneyParser: MoneyParser) : TextWatcher {
    private var isRunning = false
    private var isDeleting = false

    override fun beforeTextChanged(charSequence: CharSequence, start: Int, count: Int, after: Int) {
        isDeleting = count > after
    }

    override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(editable: Editable) {
        if (isRunning || isDeleting) {
            return
        }
        isRunning = true

        val value = editable.toString().removeNonNumbers().toDouble()/100

        val currentText = moneyParser.format(value)

        editable.clear()
        editable.append(currentText)

        isRunning = false
    }

}