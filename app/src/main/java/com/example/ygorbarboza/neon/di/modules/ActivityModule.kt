package com.example.ygorbarboza.neon.di.modules

import android.app.Activity
import com.example.ygorbarboza.neon.screen.Navigation
import dagger.Module
import dagger.Provides

@Module
abstract class ActivityModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        internal fun provideNavigation(activity: Activity) = Navigation(activity)
    }

}