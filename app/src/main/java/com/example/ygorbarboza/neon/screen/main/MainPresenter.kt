package com.example.ygorbarboza.neon.screen.main

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import com.example.ygorbarboza.neon.MAIN_USER
import com.example.ygorbarboza.neon.RxSchedulers
import com.example.ygorbarboza.neon.applySchedulerIO
import com.example.ygorbarboza.neon.ext.addToComposite
import com.example.ygorbarboza.neon.screen.Navigation
import com.example.ygorbarboza.neon.usecase.TokenUseCase
import com.example.ygorbarboza.neon.screen.pattern.presenter.Presenter
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainPresenter @Inject
constructor(
        viewBinder: MainViewBinder,
        navigation: Navigation,
        private val tokenUseCase: TokenUseCase,
        private val rxSchedulers: RxSchedulers)
    : Presenter<MainViewBinder>(viewBinder) {

    private val compositeDisposable = CompositeDisposable()

    init {
        viewBinder
                .onSendMoneyButtonClick
                .subscribe { navigation.goToSendMoneyList() }
                .addToComposite(compositeDisposable)

        viewBinder.onTransferHistoryButtonClick
                .subscribe { navigation.goToTransferHistory() }
                .addToComposite(compositeDisposable)

        viewBinder.onRetryButtonClick
                .subscribe { requestToken() }
                .addToComposite(compositeDisposable)

        requestToken()
    }

    private fun requestToken() {
        //Server seems to be down

        tokenUseCase
                .requestNewToken()
                .applySchedulerIO(rxSchedulers)
                .doOnSubscribe { viewBinder.showLoading() }
                .subscribe({ viewBinder.showCurrentUser(MAIN_USER) }, { viewBinder.showError() })
                .addToComposite(compositeDisposable)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        compositeDisposable.clear()
    }

}
