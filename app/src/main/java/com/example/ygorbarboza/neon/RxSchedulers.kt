package com.example.ygorbarboza.neon

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

open class RxSchedulers {

    open fun computationScheduler() = Schedulers.computation()

    open fun ioScheduler() = Schedulers.io()

    open fun mainScheduler() = AndroidSchedulers.mainThread()

}

fun <T : Any> Single<T>.applySchedulerIO(rxSchedulers: RxSchedulers): Single<T> =
        subscribeOn(rxSchedulers.ioScheduler())
        .observeOn(rxSchedulers.mainScheduler())
        .doOnError({ throwable -> throwable.printStackTrace() })
