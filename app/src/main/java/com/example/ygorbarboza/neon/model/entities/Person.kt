package com.example.ygorbarboza.neon.model.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Person(
        val id: Int,
        val name: String,
        val email: String,
        val phone: String,
        val photoUrl: String,
        val tansferAmount: Double = 1.0) : Parcelable