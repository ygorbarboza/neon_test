package com.example.ygorbarboza.neon.screen.transfer_history

import com.example.ygorbarboza.neon.R
import com.example.ygorbarboza.neon.screen.pattern.activity.LegoControllerActivity
import javax.inject.Inject

class TransferHistoryActivity : LegoControllerActivity<TransferHistoryPresenter>() {

    override val layoutId = R.layout.activity_transfer_history

    @Inject
    override lateinit var presenter: TransferHistoryPresenter

}