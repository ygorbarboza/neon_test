package com.example.ygorbarboza.neon.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.example.ygorbarboza.neon.R
import kotlinx.android.synthetic.main.view_neon_toolbar.view.*

class NeonToolbar @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        orientation = HORIZONTAL

        LayoutInflater.from(context).inflate(R.layout.view_neon_toolbar, this, true)

        attrs?.let { checkAttibutes(it) }
    }

    private fun checkAttibutes(attrs: AttributeSet) {
        attrs.let {
            with(context.obtainStyledAttributes(attrs, R.styleable.NeonToolbar)) {
                getString(R.styleable.NeonToolbar_titleText)?.let { setTitle(it) }

                recycle()
            }
        }
    }

    fun setTitle(titleText: String) {
        titleTextView.text = titleText
    }

    fun setOnBackListener(clickListener: OnClickListener){
        backButton.setOnClickListener(clickListener)
    }

}