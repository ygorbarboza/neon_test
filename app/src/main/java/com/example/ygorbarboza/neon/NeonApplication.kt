package com.example.ygorbarboza.neon

import android.content.Context
import android.support.multidex.MultiDex
import com.example.ygorbarboza.neon.di.components.DaggerAppComponent
import dagger.android.support.DaggerApplication
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import java.io.IOException
import java.net.SocketException

class NeonApplication : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()

        RxJavaPlugins.setErrorHandler { e ->
            var exception = e

            if (exception is UndeliverableException) {
                exception = exception.cause
            }
            if (exception is IOException || exception is SocketException) {
                // fine, irrelevant network problem or API that throws on cancellation
                return@setErrorHandler
            }
            if (exception is InterruptedException) {
                // fine, some blocking code was interrupted by a dispose call
                return@setErrorHandler
            }

            Thread.currentThread().uncaughtExceptionHandler
                    .uncaughtException(Thread.currentThread(), exception)
        }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun applicationInjector() =
            DaggerAppComponent.builder().application(this).build().apply { inject(this@NeonApplication) }

}