package com.example.ygorbarboza.neon.screen.pattern.presenter

import android.arch.lifecycle.LifecycleObserver

abstract class LifecyclePresenter: LifecycleObserver
