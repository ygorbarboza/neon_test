package com.example.ygorbarboza.neon.di.modules

import android.app.Application
import android.content.Context
import android.preference.PreferenceManager
import com.example.ygorbarboza.neon.NeonApplication
import dagger.Binds
import dagger.Module
import dagger.Provides
import java.text.NumberFormat
import java.util.*

@Module
abstract class AppModule {

    @Binds
    internal abstract fun provideContext(application: Application): Context

    @Binds
    internal abstract fun provideApplication(application: NeonApplication): Application

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideSharedPreferences(application: Application) = PreferenceManager.getDefaultSharedPreferences(application)

        @Provides
        @JvmStatic
        fun provideCurrencyNumberFormat() = NumberFormat.getCurrencyInstance(Locale("pt", "BR"))
    }

}
