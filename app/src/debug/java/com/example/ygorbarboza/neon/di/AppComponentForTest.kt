package com.example.ygorbarboza.neon.di

import com.example.ygorbarboza.neon.NeonApplication
import com.example.ygorbarboza.neon.di.modules.ActivityBuilder
import com.example.ygorbarboza.neon.di.modules.AppModule
import com.example.ygorbarboza.neon.di.modules.NetworkModule
import com.example.ygorbarboza.neon.di.modules.RxJavaModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            AppModule::class,
            ActivityBuilder::class,
            NetworkModule::class,
            RxJavaModule::class
        ]
)
interface AppComponentForTest : AndroidInjector<NeonApplication> {
    @Component.Builder
    interface Builder {
        fun rxJavaModule(rxJavaModule: RxJavaModule): Builder

        fun networkModule(networkModule: NetworkModule): Builder

        @BindsInstance
        fun application(app: NeonApplication): Builder

        fun build(): AppComponentForTest
    }
}