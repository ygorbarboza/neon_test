package com.example.ygorbarboza.neon

import com.example.ygorbarboza.neon.model.rest.RequestApiService
import com.example.ygorbarboza.neon.model.storage.TokenStorage
import com.example.ygorbarboza.neon.usecase.TokenUseCase
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import io.reactivex.subscribers.TestSubscriber
import org.junit.Test

import org.junit.Assert.*

class TokenUseCaseTest {

    val requestApiService: RequestApiService = mock()
    val tokenStorage: TokenStorage = mock()

    val tokenUseCase = TokenUseCase(requestApiService, tokenStorage)

    @Test
    fun givenRequestWhenSuccessThenSaveTokenAndPassSuccess() {
        val expectedToken = "Token"

        whenever(requestApiService.generateToken(any(), any())).thenReturn(Single.just(expectedToken))

        val testSubscriber = tokenUseCase.requestNewToken().test()

        verify(tokenStorage).saveToken(eq(expectedToken))

        testSubscriber.assertValue(expectedToken)
        testSubscriber.assertComplete()
    }

    @Test
    fun givenRequestWhenErrorThenJustPassError() {
        val exception = Exception()

        whenever(requestApiService.generateToken(any(), any())).thenReturn(Single.error(exception))

        val testSubscriber = tokenUseCase.requestNewToken().test()

        verify(tokenStorage, never()).saveToken(any())

        testSubscriber.assertError(exception)
    }

}
