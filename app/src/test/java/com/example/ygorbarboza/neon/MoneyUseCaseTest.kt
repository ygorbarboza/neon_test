package com.example.ygorbarboza.neon

import com.example.ygorbarboza.neon.model.rest.RequestApiService
import com.example.ygorbarboza.neon.model.storage.TokenStorage
import com.example.ygorbarboza.neon.usecase.MoneyUseCase
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.junit.Test

class MoneyUseCaseTest {

    val token = "Token"

    val requestApiService: RequestApiService = mock()
    val tokenStorage: TokenStorage = mock {
        on { getToken() } doReturn token
    }

    val moneyUseCase = MoneyUseCase(requestApiService, tokenStorage)

    @Test
    fun givenSendMoneyWhenSuccessThenPassSuccess() {
        val user = SEND_MONEY_USER[0]
        val value = 123.0

        whenever(requestApiService.sendMoney(user.id, token, value)).thenReturn(Single.just(true))

        val testSubscriber = moneyUseCase.sendMoney(user, value).test()

        testSubscriber.assertValue(true)
        testSubscriber.assertComplete()
        thenJustCalledGetToken()
    }

    @Test
    fun givenSendMoneyWhenErrorThenPassError() {
        val user = SEND_MONEY_USER[0]
        val value = 123.0
        val exception = Exception()

        whenever(requestApiService.sendMoney(user.id, token, value)).thenReturn(Single.error(exception))

        val testSubscriber = moneyUseCase.sendMoney(user, value).test()

        testSubscriber.assertError(exception)

        thenJustCalledGetToken()
    }

    @Test
    fun givenGetTransferWhenSuccessThenPassSuccess() {
        whenever(requestApiService.getTransfers(token)).thenReturn(Single.just(SEND_MONEY_USER.toList()))

        val testSubscriber = moneyUseCase.getTransfers().test()

        testSubscriber.assertValue(SEND_MONEY_USER.toList())
        testSubscriber.assertComplete()
        thenJustCalledGetToken()
    }

    @Test
    fun givenGetTransferWhenErrorThenPassError() {
        val exception = Exception()

        whenever(requestApiService.getTransfers(token)).thenReturn(Single.error(exception))

        val testSubscriber = moneyUseCase.getTransfers().test()

        testSubscriber.assertError(exception)
        thenJustCalledGetToken()
    }

    private fun thenJustCalledGetToken() {
        verify(tokenStorage).getToken()

        verifyNoMoreInteractions(tokenStorage)
    }

//    @Test
//    fun givenRequestWhenErrorThenJustPassError() {
//        val exception = Exception()
//
//        whenever(requestApiService.generateToken(any(), any())).thenReturn(Single.error(exception))
//
//        val testSubscriber = moneyUseCase.requestNewToken().test()
//
//        verify(tokenStorage, never()).saveToken(any())
//
//        testSubscriber.assertError(exception)
//    }

}
